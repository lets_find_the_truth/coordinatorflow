//
//  SignUpViewModel.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift

class SignUpViewModel {
    let backButtonTapped = PublishSubject<Void>()
    // MARK: - Inputs
    let selectAge: AnyObserver<Void>
    
    // MARK: - Outputs
    let didSelectAge: Observable<Void>
    
    init() {
        let _selectAge = PublishSubject<Void>()
        
        self.selectAge = _selectAge.asObserver()
        self.didSelectAge = _selectAge.asObservable()
        
   
    }
}
