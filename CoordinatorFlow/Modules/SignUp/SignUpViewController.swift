//
//  SignUpViewController.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SignUpViewController: UIViewController {
    private let disposeBag = DisposeBag()
    var viewModel: SignUpViewModel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confitmPasswordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Resources.total)
        setupBindings()
    }
    
    private func setupBindings() {
        signUpButton.rx.tap.bind(to: viewModel.selectAge).disposed(by: disposeBag)
       
    }
}

extension SignUpViewController {
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParent || isBeingDismissed {
            viewModel.backButtonTapped.onNext(())
        }
    }
}
