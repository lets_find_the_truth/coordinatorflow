//
//  SignUpCoordinator.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

enum AgeChooseType {
    case age(Int)
    case cancel
}

class SignUpCoordinator: Coordinator<Void> {
    
    let disposeBag = DisposeBag()
    private let rootViewController: UINavigationController
    
    init(rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
    }
    
    override func start() -> Observable<Void> {
        let viewController = UIStoryboard(storyboardType: .LoginFlow).initFromStoryboard(viewController: SignUpViewController.self)
        rootViewController.pushViewController(viewController, animated: true)
        
        let viewModel = SignUpViewModel()
        viewController.viewModel = viewModel
        
        let backButton = viewModel.backButtonTapped.map { return () }
        
        return Observable.merge(backButton).take(1)
        
    }
}

