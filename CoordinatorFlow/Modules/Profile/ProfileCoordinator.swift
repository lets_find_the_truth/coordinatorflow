//
//  ProfileCoordinator.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

class ProfileCoordinator: BaseCoordinator<Void>{

    private let rootViewController: UIViewController
    
    init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    override func start() -> Observable<Void> {
        let viewController = UIStoryboard(storyboardType: .Profile).initFromStoryboard(viewController: ProfileViewController.self)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let viewModel = ProfileViewModel()
        viewController.viewModel = viewModel
        
        rootViewController.present(navigationController, animated: true)
        
        return Observable.never()
    }
}

