//
//  ProfileViewController.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProfileViewController: UIViewController {
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var logOutButton: UIButton!
    
    private let disposeBag = DisposeBag()
    var viewModel: ProfileViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    private func setupBindings() {
        
    }
}
