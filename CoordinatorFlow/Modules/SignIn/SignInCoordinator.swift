//
//  SignInCoordinator.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

class SignInCoordinator: Coordinator<Void> {
    private let window: UIWindow
    private let disposeBag: DisposeBag = DisposeBag()
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() -> Observable<Void> {
        let viewController = UIStoryboard(storyboardType: .LoginFlow).initFromStoryboard(viewController: SignInViewController.self)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let viewModel = SignInViewModel()
        viewController.viewModel = viewModel
        
        viewModel.showSignUp.subscribe(onNext: {
            [weak self] in self?.showSignUpScreen(rootViewController: navigationController)
        }).disposed(by: disposeBag)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        return Observable.empty()
    }
    
    private func showSignUpScreen(rootViewController: UINavigationController) {
        let signUpC = SignUpCoordinator(rootViewController: rootViewController)
        coordinate(to: signUpC).subscribe().disposed(by: signUpC.disposeBag)
    }
}
