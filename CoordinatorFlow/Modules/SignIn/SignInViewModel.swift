//
//  SignInViewModel.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift

class SignInViewModel {
    // MARK: - Inputs
    let tapSignUp: AnyObserver<Void>
    
    // MARK: - Outputs
    let showSignUp: Observable<Void>
    
    init() {
        let _tapSignUP = PublishSubject<Void>()
        
        self.tapSignUp = _tapSignUP.asObserver()
        self.showSignUp = _tapSignUP.asObservable()
    }
}
