//
//  ProfileEditCoordinator.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

enum ProfileEditCoordinationResult {
    case age(Int)
    case cancel
} // or delete and put void

class ProfileEditCoordinator: BaseCoordinator<ProfileEditCoordinationResult>{

    private let rootViewController: UIViewController
    
    init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = UIStoryboard(storyboardType: .Profile).initFromStoryboard(viewController: ProfileEditViewController.self)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let viewModel = ProfileEditViewModel()
        viewController.viewModel = viewModel
        
        let cancel = viewModel.didCancel.map { _ in CoordinationResult.cancel }
        let age = viewModel.didSelectAge.map { CoordinationResult.age($0) }
        
        rootViewController.present(navigationController, animated: true)
        
        return Observable.merge(cancel, age)
            .take(1)
            .do(onNext: { [weak self] _ in self?.rootViewController.dismiss(animated: true) })
    }
}

