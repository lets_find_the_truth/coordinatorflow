//
//  ProfileEditViewController.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProfileEditViewController: UIViewController {
    @IBOutlet weak var agePickerView: UIPickerView!
    @IBOutlet weak var saveButton: UIButton!
    
    private let disposeBag = DisposeBag()
    var viewModel: ProfileEditViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    private func setupBindings() {
        
    }
}
