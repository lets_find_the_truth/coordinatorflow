//
//  ProfileEditViewModel.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift

class ProfileEditViewModel {

    // MARK: - Inputs
    let selectAge: AnyObserver<Int>
    let cancel: AnyObserver<Void>
    
    // MARK: - Outputs
    let ages: Observable<[Int]>
    let didSelectAge: Observable<Int>
    let didCancel: Observable<Void>
    
    init() {
        self.ages = Observable.of([1,2,3,4,5,6,7,8,9,10])
        let _selectLanguage = PublishSubject<Int>()
        self.selectAge = _selectLanguage.asObserver()
        self.didSelectAge = _selectLanguage.asObservable()
        
        let _cancel = PublishSubject<Void>()
        self.cancel = _cancel.asObserver()
        self.didCancel = _cancel.asObservable()
    }
}
