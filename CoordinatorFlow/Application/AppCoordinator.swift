//
//  AppCoordinator.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

class AppCoordinator: Coordinator<Void> {
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() -> Observable<Void> {
        coordinate(to: SignInCoordinator(window: window)).subscribe()
        return Observable.never()
    }
}

//class AppCoordinator: BaseCoordinator<Void> {
//
//    private let window: UIWindow
//
//    init(window: UIWindow) {
//        self.window = window
//    }
//
//    override func start() -> Observable<Void> {
//        let signInCoordinator = SignInCoordinator(window: window)
//        return coordinate(to: signInCoordinator)
//    }
//}
