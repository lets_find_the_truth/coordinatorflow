//
//  BaseCoordinator.swift
//  CoordinatorFlow
//
//  Created by Kyryl Nevedrov on 03/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxSwift
import Foundation

class Coordinator<ResultType> {
    typealias CoordinationResult = ResultType
    private let disposeBag = DisposeBag()
    private let identifier = UUID()
    var childCoordinators = [UUID: Any]()
    func start() -> Observable<ResultType> {
        fatalError("Start method should be implemented.")
    }
}

extension Coordinator {
    func coordinate<T>(to coordinator: Coordinator<T>) -> Observable<T> {
        childCoordinators[coordinator.identifier] = coordinator
        print(childCoordinators)
        return coordinator.start().do(onNext: { [weak self] (next) in
            self?.childCoordinators[coordinator.identifier] = nil
        })
    }
}









/// Base abstract coordinator generic over the return type of the `start` method.
class BaseCoordinator<ResultType> {
    
    /// Typealias which will allows to access a ResultType of the Coordainator by `CoordinatorName.CoordinationResult`.
    typealias CoordinationResult = ResultType
    
    /// Utility `DisposeBag` used by the subclasses.
    let disposeBag = DisposeBag()
    
    /// Unique identifier.
    private let identifier = UUID()
    
    /// Dictionary of the child coordinators. Every child coordinator should be added
    /// to that dictionary in order to keep it in memory.
    /// Key is an `identifier` of the child coordinator and value is the coordinator itself.
    /// Value type is `Any` because Swift doesn't allow to store generic types in the array.
    private var childCoordinators = [UUID: Any]()
    
    
    /// Stores coordinator to the `childCoordinators` dictionary.
    ///
    /// - Parameter coordinator: Child coordinator to store.
    private func store<T>(coordinator: BaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = coordinator
    }
    
    /// Release coordinator from the `childCoordinators` dictionary.
    ///
    /// - Parameter coordinator: Coordinator to release.
    private func free<T>(coordinator: BaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = nil
    }
    
    /// 1. Stores coordinator in a dictionary of child coordinators.
    /// 2. Calls method `start()` on that coordinator.
    /// 3. On the `onNext:` of returning observable of method `start()` removes coordinator from the dictionary.
    ///
    /// - Parameter coordinator: Coordinator to start.
    /// - Returns: Result of `start()` method.
    func coordinate<T>(to coordinator: BaseCoordinator<T>) -> Observable<T> {
        store(coordinator: coordinator)
        print(childCoordinators)
        return coordinator.start()
            .do(onNext: { [weak self] _ in
                self?.free(coordinator: coordinator)
                print("free")
                }, onCompleted: { [weak self] in
                    self?.free(coordinator: coordinator)
            })
    }
    
    /// Starts job of the coordinator.
    ///
    /// - Returns: Result of coordinator job.
    func start() -> Observable<ResultType> {
        fatalError("Start method should be implemented.")
    }
}
